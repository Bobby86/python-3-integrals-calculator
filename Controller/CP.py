import multiprocessing
import os, sys
import time
import queue
from math import *
from functools import reduce
import operator

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

class CenterPoint (multiprocessing.Process):

    def __init__(self, parameters):  # ~ const

        multiprocessing.Process.__init__(self)
        self.results = parameters['results']
        self.nDementions = parameters['nDementions']
        self.func = parameters['function']
        self.limits = parameters['limits'][:]
        self.nParts = parameters['nParts']


    def run (self):
        Integral = 0
        elapsedTime = time.time()
        Integral = self.calcIntegral ()      
        elapsedTime = time.time() - elapsedTime
        if None == self.results:
            return Integral
        self.results.put (Integral)     

    def calcIntegral (self):
        nDementions = self.nDementions
        func = self.func
        limits = self.limits[:]
        nParts = self.nParts
        steps = [ ((limits[d][1] - limits[d][0])/float(nParts)) for d in range(nDementions) ]     # O(const) # delta(x)
        coeff = reduce (operator.mul, steps, 1)   # A in theoretical part
        Xes = []
        Sum = 0.0
        # list of points in the limits of each axis devided by steps (central points)
        for d in range(nDementions):
            locXes = []
            currStep = limits[d][0]
            for i in range(nParts):
                locXes.append ( round((2.0 * currStep + steps[d]) / 2.0, 5) )   # Warning round
                currStep += steps[d]
            Xes.append (locXes)
        Sum = self.calcSum (Xes, func)
        Sum *= coeff

        return Sum

    def calcSum (self, Points, func):

        Sum = 0.0
        nDementions = len(Points)
        p = []

        code_globals = {}
        exec (func, globals(), code_globals)
        
        if 1 == nDementions:
            # linear
            for i in range(len(Points[0])):
                p.insert(0, Points[0][i])
                Sum += code_globals['f'](p)  #func (p)

        elif 2 == nDementions: 
            
            for i0 in range(len(Points[0])):
                try:
                    p.pop(0)
                except:
                    pass
                p.insert (0, Points[0][i0])

                for i1 in range(len(Points[1])):
                    try:
                        p.pop(1)
                    except:
                        pass
                    p.insert (1, Points[1][i1])
                    Sum += code_globals['f'](p)  #func (p)

        elif 3 == nDementions: 
        
            for i0 in range(len(Points[0])):
                try:
                    p.pop(0)
                except:
                    pass
                p.insert (0, Points[0][i0])

                for i1 in range(len(Points[1])):
                    try:
                        p.pop(1)
                    except:
                        pass
                    p.insert (1, Points[1][i1])

                    for i2 in range(len(Points[2])):
                        try:
                            p.pop(2)
                        except:
                            pass
                        p.insert (2, Points[2][i2])
                        Sum += code_globals['f'](p)  #func (p)

        elif 4 == nDementions: 
        
            for i0 in range(len(Points[0])):
                try:
                    p.pop(0)
                except:
                    pass
                p.insert (0, Points[0][i0])

                for i1 in range(len(Points[1])):
                    try:
                        p.pop(1)
                    except:
                        pass
                    p.insert (1, Points[1][i1])

                    for i2 in range(len(Points[2])):
                        try:
                            p.pop(2)
                        except:
                            pass
                        p.insert (2, Points[2][i2])

                        for i3 in range(len(Points[3])):
                            try:
                                p.pop(3)
                            except:
                                pass
                            p.insert (3, Points[3][i3])
                            Sum += code_globals['f'](p)  #func (p)

        elif 5 == nDementions: 
        
            for i0 in range(len(Points[0])):
                try:
                    p.pop(0)
                except:
                    pass
                p.insert (0, Points[0][i0])

                for i1 in range(len(Points[1])):
                    try:
                        p.pop(1)
                    except:
                        pass
                    p.insert (1, Points[1][i1])

                    for i2 in range(len(Points[2])):
                        try:
                            p.pop(2)
                        except:
                            pass
                        p.insert (2, Points[2][i2])

                        for i3 in range(len(Points[3])):
                            try:
                                p.pop(3)
                            except:
                                pass
                            p.insert (3, Points[3][i3])

                            for i4 in range(len(Points[4])):
                                try:
                                    p.pop(4)
                                except:
                                    pass
                                p.insert (4, Points[4][i4])
                                Sum += code_globals['f'](p)  #func (p)

        elif 6 == nDementions: 
        
            for i0 in range(len(Points[0])):
                try:
                    p.pop(0)
                except:
                    pass
                p.insert (0, Points[0][i0])

                for i1 in range(len(Points[1])):
                    try:
                        p.pop(1)
                    except:
                        pass
                    p.insert (1, Points[1][i1])

                    for i2 in range(len(Points[2])):
                        try:
                            p.pop(2)
                        except:
                            pass
                        p.insert (2, Points[2][i2])

                        for i3 in range(len(Points[3])):
                            try:
                                p.pop(3)
                            except:
                                pass
                            p.insert (3, Points[3][i3])

                            for i4 in range(len(Points[4])):
                                try:
                                    p.pop(4)
                                except:
                                    pass
                                p.insert (4, Points[4][i4])

                                for i5 in range(len(Points[5])):
                                    try:
                                        p.pop(5)
                                    except:
                                        pass
                                    p.insert (5, Points[5][i5])
                                    Sum += code_globals['f'](p)  #func (p)

        else:
            Sum = -1                                

        return Sum
