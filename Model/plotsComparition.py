import matplotlib.pyplot as plt
import matplotlib
import numpy as np

plt.figure (figsize=(11, 10))

#fig, ax = plt.subplots() 
#loc = matplotlib.ticker.MultipleLocator(base=0.1)
#ax.yaxis.set_major_locator(loc)

#plt.annotate (..., fontsize=1)
# axes and lines configuration
# plt.axis ([27, 1000, 0.0, 0.06])

font = {'family' : 'normal',
		'weight' : 'bold',
		'size'   : 22 }
matplotlib.rc ('font', **font)
plt.rc ('lines', linewidth=3)
plt.grid (True)
plt.xlabel ('amount of dimentions')
plt.ylabel ('calculation time (sec)')
plt.title ('')


plt.axis ([0, 7, 0, 2.7])
x = list (range (1, 7))


#y = [0.19, 0.19, 0.2, 0.19, 0.2, 0.19]
#plt.plot (x, y, '-og', label='MC: 10k')
y = [1.22, 1.47, 1.78, 2.03, 2.4, 2.6]
plt.plot (x, y, '-ob', label='MC: 1000k')
y = [0.22, 0.22, 0.25, 0.55, 2.7, 2.7]
plt.plot (x, y, '-or', label='CP: 0.01')
# y = [0.22, 0.22, 0.25, 0.55, 7.5, 155.5]
# plt.plot (x, y, 'o', label='CP: 0.001')
#plt.yticks (np.arange(min(y), max(y) + 1), 0.1)


plt.legend (loc=2)

plt.savefig ('plots01.png', format = 'png')
# show picture
# plt.show()
