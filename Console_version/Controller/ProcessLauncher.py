#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 12:27:57 2014

@author: V.Rybnikov IS-16
"""

import multiprocessing
import os, sys
import queue
import functools
import time
import random
from math import *

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from Controller.MC import MonteCarlo
from Controller.CP import CenterPoint


class ProcessLauncher:

    """
    this class run MonteCarlo(MC) and/or CenterPoint(CP) methods as linear or parallel 
    """

    def __init__(self): #, limits, func, mc, cp, nBranches = 0):
        """
        limits - the integration limits: list of pairs [A, B] where A bottom border of integral and B is top border;
        function - integrand, string that will be executed as python code; 
        MonteCarlo[0], CenterPoint[0] - run this method if True; 
        MonteCarlo[1] - number of tests for MC method;
        CenterPoint[1] - precision(epsilon) for CP method;
        nBranches - number of threads per method;
        """
    def run (self, arguments):
        
        self.parameters = { 'limits': arguments['limits'], 'function': arguments['function'], 
                            'MonteCarlo': arguments['MonteCarlo'][0], 'mcTests': arguments['MonteCarlo'][1], 
                            'CenterPoint': arguments['CenterPoint'][0], 'precision': arguments['CenterPoint'][1], 
                            'nBranches': arguments['nBranches'] }
                           
        resMC, resCP = 0.0, 0.0
        MCtime, CPtime = 0.0, 0.0        

        if self.parameters['MonteCarlo']:
            MCtime = time.time()
            resMC = self.runMC()
            MCtime = time.time() - MCtime
        if self.parameters['CenterPoint']:
            CPtime = time.time()         
            resCP = self.runCP()
            CPtime = time.time() - CPtime
        # print(MCtime)
        return [ [ MCtime, resMC ], [ CPtime, resCP ] ]


#   Monte-Carlo method
    def runMC (self):

        nTests = self.parameters['mcTests']
        limits = self.parameters['limits']
        numCores = self.parameters['nBranches']
        func = self.parameters['function']

        threads = []
        parallelFlag = False

        # if nBranches: numCores = nBranches
        # else: numCores = multiprocessing.cpu_count()  # const
        # numCores = 1
        iterationsPerThread = int (nTests / numCores);  # const

        # pre-calculations: // results will be used by every thread
        nDementions = len (limits)     # O(n) ~ n isn't big value   ~ const
        # pre-calculation (b[i] - a[i]) from limits // small amount of dementions 
        preLimits = [ (border[1] - border[0]) for border in limits ]  # O( const * n )  ~ const
        preIntCoeff = functools.reduce (lambda x, y: x*y, preLimits) / iterationsPerThread # O( const * n )

        queueOfResults = multiprocessing.Queue ()
        random.seed()   #random init

        arguments = [queueOfResults, iterationsPerThread, nDementions, preLimits, preIntCoeff, func, limits[:]]
        if (1 < numCores):
            parallelFlag = True
            for proc in range(numCores - 1):  # 1 core left for main thread
                t = MonteCarlo (arguments)
                t.start ()
                threads.append(t)
        # calculate iterationsPerThread tests in the main thread
        arguments[0] = None  # flag for main thread
        mainThread = MonteCarlo (arguments) # const
        mainResult = mainThread.run()

        if parallelFlag:
            for t in threads:
                    t.join()
            results = [queueOfResults.get() for t in threads]
            return (fsum (results) + mainResult) / numCores  # avg from each thread (math.fsum) #  ~ const
        # else
        return mainResult

#   Central Point method
    def calcSumCP (self, arguments):

        limits = arguments['limits']
        nBranches = arguments['nBranches']
        nParts = arguments['nParts']
        func = arguments['function']
        threads = []
        Integral = 0    #calculaed value per test
        parallelFlag = False

        if nBranches: numCores = nBranches
        else: numCores = multiprocessing.cpu_count()  # const
        # numCores = 1

        # pre-calculations: // results will be used by every thread
        nDementions = len (limits)     # O(n) ~ n isn't big value   ~ const
        queueOfResults = multiprocessing.Queue ()
        tmpLimits = limits[:]
        # nParts = [ ((limits[d][1] - limits[d][0]) / 10) for d in range(nDementions)] # a step for each axis
        arguments = { 'results': queueOfResults, 'nDementions': nDementions, 'function': func, 'limits': tmpLimits, 'nParts': nParts}

        if (1 < numCores):
            parallelFlag = True
            limitsParts = [ ((d[1] - d[0]) / float(numCores)) for d in limits ]    # size of parts of limits per branch # WARNING
            # print ('limitsParts: ', limitsParts)
            currA, currB = 0.0, 0.0
            for procID in range(numCores - 1):  # 1 core left for main thread

                # get a part of the limits for each branch(current procID) from each axis(demention)
                for d in range(nDementions):
                    currA = limits[d][0] + procID * limitsParts[d]
                    currB = currA + limitsParts[d]
                    arguments['limits'][d] = [currA, currB]

                # create and start a branch
                t = CenterPoint (arguments)
                t.start ()
                threads.append(t)
            arguments['limits'] = [ [arguments['limits'][d][1], float(limits[d][1])] for d in range(nDementions) ] 
        # calculate last part in the main thread
        arguments['results'] = None  # flag for main thread
        mainThread = CenterPoint (arguments) # const
        mainResult = mainThread.run()

        if parallelFlag:
            for t in threads:
                    t.join()
            results = [queueOfResults.get() for t in threads]
            # print (fsum(results), mainResult)
            return (fsum (results) + mainResult) * (2**(nDementions-1)) # sum of results from each thread (math.fsum) #  ~ const
        # else
        return mainResult

    def runCP (self):

        epsilon = self.parameters['precision']
        limits = self.parameters['limits']
        nBranches = self.parameters['nBranches']
        func = self.parameters['function']
        nParts = 10 #TODO # parts in each pair of the limits
        Sum = [0, 0]
        arguments = { 'limits':limits, 'nBranches': nBranches, 'nParts': nParts, 'function': func }

        Sum[0] = self.calcSumCP (arguments)
        arguments['nParts'] *= 2
        Sum[1] = self.calcSumCP (arguments)

        sumIndex = 1
        while fabs(Sum[1] - Sum[0]) > epsilon:
            if sumIndex: sumIndex = 0
            else: sumIndex = 1
            arguments['nParts'] *= 2
            Sum[sumIndex] = self.calcSumCP (arguments)

        return Sum[sumIndex]


# main
if __name__ == '__main__':

    nTests = 1000000    # amount of tests
    # limits = [[3, 7], [10, 56], [9, 25], [10, 20], [10, 20]]#, [30, 50]]          #limits of integration
    limits = [[5, 11], [5, 11], [9, 17]]          #limits of integration
    # theorVal = 4716288   #theoretical value of integral

    func = input ('Input the integrand: ')  # input integrated function in python-style code

    func = """def f(x): return """ + func   # code of new dynamic object-function

    # pl = ProcessLauncher (limits, func, [True, nTests], [False, 0])
    arguments = {  'limits': limits, 'function': func, 'MonteCarlo': [True, 1000000], 
                   'CenterPoint': [False, 10000], 'nBranches': 0 }
    pl = ProcessLauncher ()

    try:
        res = pl.run(arguments)
        print ('MC result: ', res[0][0])
        print ('MC time: ', res[0][1])
        print ('CP result: ', res[1][0])
        print ('CP time: ', res[1][1])
        # print (res)
    except ValueError as e:
        print (e)
    except:
        print ('Unknown error from ProcessLauncher object: ', sys.exc_info()[0])
