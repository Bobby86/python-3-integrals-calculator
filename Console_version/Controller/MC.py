#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 12:27:57 2014

@author: V.Rybnikov IS-16
"""

import multiprocessing
import os, sys
import time
import random
import queue
from math import *

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

class MonteCarlo (multiprocessing.Process):

    def __init__(self, parameters):  # ~ const
        multiprocessing.Process.__init__(self)
        self.results = parameters[0]            # shared queue 
        self.nTests = parameters[1]             
        self.nDementions = parameters[2]
        self.preLimits = parameters[3]
        self.preIntCoeff = parameters[4]        # outer integral coefficent
        self.func = parameters[5]
        self.limits = parameters[6]


    def run (self):

        # print('MC')
        # print('run process #', self.name, sep='')
        Integral = 0
        code_globals = {}
        exec (self.func, globals(), code_globals)
        # elapsedTime = time.time()

        for test in range (self.nTests): # O(nTests) nTests is big value
            t = random.random() # const
            x = [ (self.limits[i][0] + self.preLimits[i] * t) for i in range (self.nDementions) ] # const
            Integral += code_globals['f'](x)                  

        # elapsedTime = time.time() - elapsedTime
        # print (elapsedTime)

        Integral = (self.preIntCoeff) * Integral    # const

        if None == self.results:
            return Integral

        self.results.put (Integral)#[Integral, self.name, elapsedTime])
