import multiprocessing
import os, sys
import time
import queue
from math import *
from functools import reduce
import operator

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from Controller.CalcSum import calcSum

class CenterPoint (multiprocessing.Process):

    def __init__(self, parameters):  # ~ const

        multiprocessing.Process.__init__(self)
        self.results = parameters['results']
        self.nDementions = parameters['nDementions']
        self.func = parameters['function']
        self.limits = parameters['limits'][:]
        self.nParts = parameters['nParts']


    def run (self):

        # print('CP')
        # print('run process #', self.name, sep='')
        Integral = 0

        elapsedTime = time.time()
        Integral = self.calcIntegral ()      
        elapsedTime = time.time() - elapsedTime
        # print ('time: ', elapsedTime)

        if None == self.results:
            return Integral

        self.results.put (Integral)     #[Integral, elapsedTime])

    def calcIntegral (self):

        nDementions = self.nDementions
        func = self.func
        limits = self.limits[:]
        # print('limits: ', limits)
        #TODO
        nParts = self.nParts

        steps = [ ((limits[d][1] - limits[d][0])/float(nParts)) for d in range(nDementions) ]     # O(const) # delta(x)
        coeff = reduce (operator.mul, steps, 1)   # A in theoretical part
        Xes = []
        Sum = 0.0

        # list of points in the limits of each axis devided by steps (central points)
        for d in range(nDementions):
            locXes = []
            currStep = limits[d][0]
            for i in range(nParts):
                locXes.append ( round((2.0 * currStep + steps[d]) / 2.0, 5) )   # Warning round
                currStep += steps[d]
            Xes.append (locXes)
        # print ('Points: ', Xes)
        Sum = calcSum (Xes, func)

        Sum *= coeff
        # print('coeff: ', coeff)
        # print('Sum: ', Sum)
        return Sum