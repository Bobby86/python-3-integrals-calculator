"""
    calculation of Sum for Center Point method 
    max dementions is 6
"""

import os, sys
from math import *

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

def calcSum (Points, func):

    Sum = 0.0
    nDementions = len(Points)
    p = []

    code_globals = {}
    exec (func, globals(), code_globals)
    
    if 1 == nDementions:
        # linear
        for i in range(len(Points[0])):
            p.insert(0, Points[0][i])
            Sum += code_globals['f'](p)  #func (p)

    elif 2 == nDementions: 
        
        for i0 in range(len(Points[0])):
            try:
                p.pop(0)
            except:
                pass
            p.insert (0, Points[0][i0])

            for i1 in range(len(Points[1])):
                try:
                    p.pop(1)
                except:
                    pass
                p.insert (1, Points[1][i1])
                Sum += code_globals['f'](p)  #func (p)

    elif 3 == nDementions: 
    
        for i0 in range(len(Points[0])):
            try:
                p.pop(0)
            except:
                pass
            p.insert (0, Points[0][i0])

            for i1 in range(len(Points[1])):
                try:
                    p.pop(1)
                except:
                    pass
                p.insert (1, Points[1][i1])

                for i2 in range(len(Points[2])):
                    try:
                        p.pop(2)
                    except:
                        pass
                    p.insert (2, Points[2][i2])
                    Sum += code_globals['f'](p)  #func (p)

    elif 4 == nDementions: 
    
        for i0 in range(len(Points[0])):
            try:
                p.pop(0)
            except:
                pass
            p.insert (0, Points[0][i0])

            for i1 in range(len(Points[1])):
                try:
                    p.pop(1)
                except:
                    pass
                p.insert (1, Points[1][i1])

                for i2 in range(len(Points[2])):
                    try:
                        p.pop(2)
                    except:
                        pass
                    p.insert (2, Points[2][i2])

                    for i3 in range(len(Points[3])):
                        try:
                            p.pop(3)
                        except:
                            pass
                        p.insert (3, Points[3][i3])
                        Sum += code_globals['f'](p)  #func (p)

    elif 5 == nDementions: 
    
        for i0 in range(len(Points[0])):
            try:
                p.pop(0)
            except:
                pass
            p.insert (0, Points[0][i0])

            for i1 in range(len(Points[1])):
                try:
                    p.pop(1)
                except:
                    pass
                p.insert (1, Points[1][i1])

                for i2 in range(len(Points[2])):
                    try:
                        p.pop(2)
                    except:
                        pass
                    p.insert (2, Points[2][i2])

                    for i3 in range(len(Points[3])):
                        try:
                            p.pop(3)
                        except:
                            pass
                        p.insert (3, Points[3][i3])

                        for i4 in range(len(Points[4])):
                            try:
                                p.pop(4)
                            except:
                                pass
                            p.insert (4, Points[4][i4])
                            Sum += code_globals['f'](p)  #func (p)

    elif 6 == nDementions: 
    
        for i0 in range(len(Points[0])):
            try:
                p.pop(0)
            except:
                pass
            p.insert (0, Points[0][i0])

            for i1 in range(len(Points[1])):
                try:
                    p.pop(1)
                except:
                    pass
                p.insert (1, Points[1][i1])

                for i2 in range(len(Points[2])):
                    try:
                        p.pop(2)
                    except:
                        pass
                    p.insert (2, Points[2][i2])

                    for i3 in range(len(Points[3])):
                        try:
                            p.pop(3)
                        except:
                            pass
                        p.insert (3, Points[3][i3])

                        for i4 in range(len(Points[4])):
                            try:
                                p.pop(4)
                            except:
                                pass
                            p.insert (4, Points[4][i4])

                            for i5 in range(len(Points[5])):
                                try:
                                    p.pop(5)
                                except:
                                    pass
                                p.insert (5, Points[5][i5])
                                Sum += code_globals['f'](p)  #func (p)

    else:
        Sum = -1                                

    return Sum
