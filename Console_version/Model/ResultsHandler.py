#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 12:27:57 2014

@author: V.Rybnikov IS-16
"""

import os, sys, traceback
from math import *
import multiprocessing

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from Controller.ProcessLauncher import ProcessLauncher

class ResultsHandler:

    """
    this class store and manage result data from View and Controller 
    """

    def __init__(self): #, limits, func, mc, cp, nBranches = 0):
        """
        """
        self.lastSpeedupTest = []   # [..., [nTests, Linear[Time, Result], Parallel[Time, Result], Speedup], ...]
        self.lastMCvsCPTest = []    # [..., [presicion, MC[time, value], CP[time, value]], ...]
        self.outMCStestFName ='MCSpeedupTest10k-100k'
        self.outMCvsCPStestFName = 'MCvsCPTest'


    def run (self, arguments):

        """
        this method check input arguments from View module and run selected tests
        """

        try:
            self.argsCheck (arguments)
        except KeyError:
            raise KeyError('AppError: uncorrect parameters (KeyError)')
        except ValueError as e:
            raise ValueError(e)
        except:
            raise ValueError('AppError: unknown error ' + str(sys.exc_info()[0]))

        if not arguments['nBranches']: 
            arguments['nBranches'] = multiprocessing.cpu_count()  # const

        tmpArgs = arguments.copy()

        pl = ProcessLauncher ()
        tmpArgs['PLobj'] = pl

        # run SpeedUp MC tests
        if arguments['MCSpeedup']:

            tmpArgs['MonteCarlo'][0] = True
            if arguments['nBranches'] == 1:
                raise ValueError('InputError: cannot run Speedup test for 1 branch')
            self.runMCSpeedUpTest(tmpArgs)
            tmpArgs['MonteCarlo'][0] = False
            tmpArgs['CenterPoint'][0] = False
            tmpArgs['MCvsCP'] = False
        
        # run MC vs CP comparition test
        if arguments['MCvsCP']:
            
            tmpArgs['CenterPoint'][0] = True
            tmpArgs['MonteCarlo'][0] = True
            self.runMCvsCPtest(tmpArgs)
            tmpArgs['CenterPoint'][0] = False
            tmpArgs['MonteCarlo'][0] = False

        if arguments['MonteCarlo'][0]:

            tmpArgs['CenterPoint'][0] = False
            self.runMC(tmpArgs)

        if arguments['CenterPoint'][0]:

            tmpArgs['MonteCarlo'][0] = False
            self.runCP(tmpArgs)


    def runMCSpeedUpTest (self, arguments):

        """
        this method run linear and parallel Monte-Carlo method realisations for equal amount of tests,
        and write results to this object data (lastSpeedupTest) and output file
        """

        tmpArgs = arguments.copy()
        pl = arguments['PLobj']
        tmpArgs['CenterPoint'][0] = False   # to be sure
        self.lastSpeedupTest = []
        try:
            outMCStest = open (self.outMCStestFName, 'w')

            for test in arguments['MonteCarlo'][1]:

                tmpArgs['MonteCarlo'][1] = test
                # run MC method for current test
                tmpArgs['nBranches'] = 1
                resLinear = pl.run (tmpArgs)     # get calculation time and integral value from linear MC realisation
                tmpArgs['nBranches'] = arguments['nBranches']
                resParallel = pl.run (tmpArgs) # get calculation time and integral value from parralel MC realisation
                # write results to the file (nTests time integralValue)
                outMCStest.write (str(test) + ' ' + str(resParallel[0][0]) + ' ' + str(resLinear[0][0]) + '\n')
                speedup = 0
                if resLinear[0][0] and resParallel[0][0]:
                    speedup = resLinear[0][0] / resParallel[0][0]
                else:
                    print ('AppError: can\'t calculate speedup')
                self.lastSpeedupTest.append ([test, resParallel, resLinear, speedup])
                print ('amount of tests: ', test)
                print ('MC result parralel: ', resParallel[0][1])
                print ('MC result linear:   ', resLinear[0][1])
                print ('MC time parralel:   ', resParallel[0][0])
                print ('MC time linear:     ', resLinear[0][0])
                print ('--------------------------------------')

        except (ValueError, TypeError, NameError, IndexError) as e:
                print ('AppError in ProcessLauncher object: ', e) 
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)      
        except IOError as e:
                print ('AppError: cannot use the output file', e)
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)   
        except:
                print ('Unknown error from ProcessLauncher object: ', sys.exc_info()[0])
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)   
        finally:
            if not outMCStest.closed:
                outMCStest.close()


    def runMCvsCPtest (self, arguments):

        tmpArgs = arguments.copy()
        tmpArgs['MonteCarlo'][0] = True   # to be sure
        tmpArgs['CenterPoint'][0] = True   # to be sure
        MCarg = arguments['MonteCarlo'][1]
        CParg = arguments['CenterPoint'][1]
        pl = arguments['PLobj']
        self.lastMCvsCPTest = []
        try:
            outMCvsCPStest = open (self.outMCvsCPStestFName, 'w')

            for testIndex in range(len(arguments['MonteCarlo'][1])):

                tmpArgs['MonteCarlo'][1] = MCarg[testIndex]
                tmpArgs['CenterPoint'][1] = CParg[testIndex]
                res = pl.run (tmpArgs)
                # put precision + MCtime + CPtime to the output file
                outMCvsCPStest.write (str( tmpArgs['CenterPoint'][1] ) + ' ' + str( res[0][0] ) + ' ' + str( res[1][0] ) + '\n')
                self.lastMCvsCPTest.append ([tmpArgs['CenterPoint'][1], res[0], res[1]])
                print ('precision: ', tmpArgs['CenterPoint'][1])
                print ('MC time:   ', res[0][0])
                print ('Cp time:   ', res[1][0])
                print ('MC result: ', res[0][1])
                print ('CP result: ', res[1][1])
                print ('----------------------------------------------')

        except (ValueError, TypeError, NameError, IndexError) as e:
                print ('AppError in ProcessLauncher object: ', e) 
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)      
        except IOError as e:
                print ('AppError: cannot use the output file', e)
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)  
        except:
                print ('Unknown error from ProcessLauncher object: ', sys.exc_info()[0])
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)   
        finally:
            if not outMCvsCPStest.closed:
                outMCvsCPStest.close()


    def runCP(self, arguments):

        tmpArgs = arguments.copy()
        CParg = arguments['CenterPoint'][1]
        tmpArgs['MonteCarlo'][0] = False   # to be sure
        pl = arguments['PLobj']
        self.lastMCvsCPTest = []
        try:
            for precision in CParg:
                tmpArgs['CenterPoint'][1] = precision 
                res = pl.run(tmpArgs)
                print ('precision: ', precision)
                print ('Cp time:   ', res[1][0])
                print ('CP result: ', res[1][1])
                print ('----------------------------------------------')
        except (ValueError, TypeError, NameError, IndexError) as e:
                print ('AppError in ProcessLauncher object: ', e) 
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)      
        except IOError as e:
                print ('AppError: cannot use the output file', e)
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)  
        except:
                print ('Unknown error from ProcessLauncher object: ', sys.exc_info()[0])
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)   


    def runMC(self, arguments):

        tmpArgs = arguments.copy()
        MCarg = arguments['MonteCarlo'][1]
        tmpArgs['CenterPoint'][0] = False   # to be sure
        pl = arguments['PLobj']
        try:
            for test in MCarg:
                tmpArgs['MonteCarlo'][1] = test
                res = pl.run(tmpArgs)
                print ('amount of tests: ', test)
                print ('MC time:   ', res[0][0])
                print ('MC result: ', res[0][1])
                print ('----------------------------------------------')
        except (ValueError, TypeError, NameError, IndexError) as e:
                print ('AppError in ProcessLauncher object: ', e) 
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)      
        except IOError as e:
                print ('AppError: cannot use the output file', e)
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)  
        except:
                print ('Unknown error from ProcessLauncher object: ', sys.exc_info()[0])
                print('-'*60)
                traceback.print_exc (file = sys.stdout)
                print('-'*60)   


    def argsCheck (self, arguments):

        flag = False
        # MC or CP flags check
        if not  (isinstance(arguments['MonteCarlo'][0], bool) or isinstance(arguments['CenterPoint'][0], bool) or
                isinstance(arguments['MCSpeedup'][0], bool) or isinstance(arguments['MCvsCP'][0], bool) or
                (arguments['CenterPoint'][0] in [0,1]) or (arguments['MonteCarlo'][0] in [0, 1]) or
                (arguments['MCSpeedup'] in [0,1]) or (arguments['MCvsCP'] in [0, 1])):
            raise ValueError('AppError: wrong MC or CP checkbox value')
        # number of slected methods or tests check, must be one
        # if (arguments['MonteCarlo'][0])
        # MC number of tests(iterations) check
        if arguments['MonteCarlo'][0] or arguments['MCvsCP'] or arguments['MCSpeedup']:
            flag = True
            if not isinstance (arguments['MonteCarlo'][1], list):
                raise ValueError('AppError: number of tests in the Monte-Carlo method is not valid')
            for test in arguments['MonteCarlo'][1]:
                if not (isinstance(test, int) and (test > 0)):
                    raise ValueError('InputError: tests for the Monte-Carlo method should be positive integers')                 
        # CP precision check
        if arguments['CenterPoint'][0] or arguments['MCvsCP']:
            flag = True
            if not isinstance (arguments['CenterPoint'][1], list):
                raise ValueError('AppError: number of precisions in the Central Point method is not valid')
            for test in arguments['CenterPoint'][1]:
                if not (isinstance(test, (int, float)) and (test > 0)):
                    raise ValueError('InputError: precisions for the Central Point method should be positive numbers')                 
        # no one of methods was choosen
        if not flag: raise ValueError('InputError: select one or both methods please')
        # number of branches variable(nBranches) validation
        if not (isinstance(arguments['nBranches'], int) and arguments['nBranches'] >= 0):
            raise ValueError('InputError: number of branches is not valid (note: input \'1\' or more please, \
                                or leave this field emty, then system use 1 branch for each processor)') 
        # comparition tests arguments check
        if arguments['MonteCarlo'][0] and arguments['CenterPoint'][0]:
            if len(arguments['MonteCarlo'][1]) != len(arguments['CenterPoint'][1]):
                raise ValueError('InputError: amount of tests for the Monte-Carlo method should be equal to \
                                  amount of precisions for the Center Point method')
        # limits list check
        if not isinstance (arguments['limits'], list):
            raise ValueError('AppError: the integration limits must be a list')
        else:
            if not len(arguments['limits']):
                raise ValueError('InputError: input the limits please')
            for l in arguments['limits']:
                if not (isinstance(l, list) and (2 == len(l)) and (isinstance(l[0], (int, float))) and 
                        (isinstance(l[1], (int, float))) and (l[1] - l[0] > 0)):
                    raise ValueError('InputError: input the limits right way please')
        # function validation
        try:
            code_globals = {}
            exec (arguments['function'], globals(), code_globals)#TODO
            p = [arguments['limits'][i][0] for i in range(len(arguments['limits']))]
            code_globals['f'](p)
        except:
            raise ValueError('InputError: function or number of limits is not valid')




# main
if __name__ == '__main__':

    MCS, MCvsCP, MC, CP = False, False, False, False
    precisions = 0
    nTests = 0

    inp = input('run MC speedup test [y/n]:')
    if inp == 'y' or inp == 'Y':
        MCS = True

    if not MCS:
        inp = input('run MC and CP comparison test [y/n]:')
        if inp == 'y' or inp == 'Y':
            MCvsCP = True

    if not (MCS or MCvsCP):
            inp = input('run Monte-Carlo method [y/n]:')
            if inp == 'y' or inp == 'Y':
                MC = True

    if not (MCS or MCvsCP or MC):
                inp = input('run Central Points method [y/n]:')
                if inp == 'y' or inp == 'Y':
                    CP = True
                else:
                    print('nothing was selected')
                    sys.exit()


    lims = input ('Input the limits of integrations: ')
    lims = lims.split(';')
    limits = []
    for lim in lims:
        lim = lim.split(',')
        try:
            if lim:
#                print('lim:', lim)
                lim[0] = float(lim[0])
                lim[1] = float(lim[1])
            else: continue
        except:
            print ('InputError: input the limits right way please')
            sys.exit()
        limits.append (lim)
#    print (limits)

    if CP or MCvsCP:
        precisions = input('Input the precision(s) list for central points method: ')
        precisions = precisions.split(',')
        try:
            if precisions:
                precisions = [float(p) for p in precisions if p]
        except:
            print('InputError: uncorrect precisions list')
            sys.exit()

    if MC or MCvsCP or MCS:
        nTests = input('Input numbers of tests list for MC method: ')
        nTests = nTests.split(',')
        try:
            if nTests:
                nTests = [int(p) for p in nTests if p]
        except:
            print('InputError: uncorrect MC tests list')
            sys.exit()

    func = input ('Input the integrand: ')  # input integrated function in python-style code
    func = """def f(x): return """ + func   # code of new dynamic object-function

    arguments = {'limits': limits, 'function': func, 'nBranches': 0, 
                 'MonteCarlo': [MC, nTests], 'CenterPoint': [CP, precisions], 
                 'MCSpeedup': MCS, 'MCvsCP': MCvsCP }

    rh = ResultsHandler ()
    try:
        rh.run(arguments)
    except ValueError as e:
        print (e)
    except:
        print ('Unknown error from ResultsHandler object: ', sys.exc_info()[0])
        print('-'*60)
        traceback.print_exc (file = sys.stdout)
        print('-'*60)   