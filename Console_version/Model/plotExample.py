

import matplotlib.pyplot as plt

plt.figure (1)
#
#axes = fig.add_axes ([0.1, 0.1, 0.8, 0.8])
#
#axes.plot (x, y, 'r')
lx = [x for x in range(10)]
ly = [x**2 for x in lx]

xmin = min(lx)
xmax = max(lx)
ymin = min(ly)
ymax = max(ly)

plt.plot (lx, ly, 'r')
plt.axis ([xmin, xmax, ymin, ymax])
plt.grid(True)
plt.xlabel ('x')
plt.ylabel ('y')
plt.title ('title')


plt.show()