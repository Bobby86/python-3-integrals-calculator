#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 16 12:27:57 2014

@author: V.Rybnikov IS-16
"""

import os, sys, traceback

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import matplotlib.pyplot as plt
import matplotlib
import numpy as np

class PlotMCSpeedup:
    
    def __init__(self):
        pass
    
    def plot(self, fileName):
        
        plt.figure (figsize=(8, 8))
        
        font = {'family' : 'normal',
        		'weight' : 'bold',
        		'size'   : 22 }
        matplotlib.rc ('font', **font)
        
        plt.rc ('lines', linewidth=3)
        plt.grid (True)
        plt.xlabel ('amount of tests')
        plt.ylabel ('calculation time (sec)')
        plt.title ('MC speedup test')
        plt.ticklabel_format (style='sci', axis='x', scilimits=(0,0))

        x, yParallel, yLinear = self.readFile (fileName)
        xMin = min(x)
        xMax = max(x)
        yMin = min (min (yParallel), min (yLinear))
        yMax = max (max (yParallel), max (yLinear))
        
        plt.axis ([xMin, xMax, yMin, yMax])
        
        
        y = yParallel
        plt.plot (x, y, '-og', label='parallel')
        y = yLinear
        plt.plot (x, y, '-or', label='linear')
        
        plt.legend (loc=4)
        plt.savefig ('MCSpeedupTest10k-100k.png', format = 'png')
        # show picture
        # plt.show()
    
    def readFile(self, fname):
        """
        read parameters from the file to the lists (nTests, timeLinear, timeParallel)
        """
        nTests = []
        tLinear = []
        tParallel = []

        inpFile = open(fname, 'r')        
        try:
            for line in inpFile:
                    tmp = line.split (' ')
#                    print (tmp)
                    nTests.append (int(tmp[0]))
                    tParallel.append ( round( float(tmp[1]), 3 ))
                    tLinear.append ( round( float(tmp[2]), 3 ))
        except:
            pass
        finally:
            if not inpFile.closed:
                inpFile.close()
        if not (len(nTests) == len(tLinear) and len(nTests) == len(tLinear)):
            raise ValueError ('AppError: error occured while reading file')
#        print (nTests, tParallel, tLinear)
        return nTests, tParallel, tLinear
        
# main
if __name__ == '__main__':

    plot = PlotMCSpeedup ()
    
    try:
        plot.plot ('MCSpeedupTest10k-100k')
    except:
        print('-'*60)
        traceback.print_exc (file = sys.stdout)
        print('-'*60)           
        