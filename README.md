# README #

* My bachelor's work for the university
* Parallel calculator for multi-dimensional integrals
* using classic and Monte-Carlo algorithms

### What is this repository for? ###

* Demonstration

### How do I get set up? ###

* Requires Python 3.4, matplotlib, PyQt4 
* Or just install WinPython
* Run View/View.py