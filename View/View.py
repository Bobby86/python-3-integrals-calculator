

import sys, os, traceback

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from PyQt4 import QtCore
from PyQt4 import QtGui

from Model import ResultsHandler, plots
                
class GUI (QtGui.QWidget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        # self.Player = Logic.PlayerService ()
        # self.PlistData = Model.Playlist()
        self.calculatorObject = ResultsHandler.ResultsHandler()
        self.plotMCS = plots.PlotMCSpeedup ()
        self.arguments = {'limits': [], 'function': '', 'nBranches': 0, 
                     'MonteCarlo': [False, []], 'CenterPoint': [False, []], 
                     'MCSpeedup': False, 'MCvsCP': False }       

       #---------general window --------------
        self.setGeometry (300, 300, 500, 350)
        self.setFixedSize (500, 700)
        self.setWindowTitle ('the Programm')
        pal = self.palette()
        pal.setColor(QtGui.QPalette.Normal, QtGui.QPalette.Window, QtGui.QColor("#009999"))
        pal.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Window, QtGui.QColor("#009999"))
        self.setPalette(pal)
        # self.setWindowIcon(QtGui.QIcon('bmo_icon.gif'))
#        self.setToolTip ('useless tooltip')
#        QtGui.QToolTip.setFont (QtGui.QFont ('OldEnglish', 10))
#        self.setWindowIcon ('path')

        #---------input fields---------------------------
 
        #---------selection part radio buttons-----------
        self.methodSelection = QtGui.QHBoxLayout()
        self.methSelectionRadioGroup = QtGui.QButtonGroup (self.methodSelection)
        self.mc = QtGui.QRadioButton ('Monte-Carlo')
        self.cp = QtGui.QRadioButton ('Rectangles(CP)')
        self.mcst = QtGui.QRadioButton ('MC Speedup Test')
        self.mcvscp = QtGui.QRadioButton ('comparison test')
        self.methSelectionRadioGroup.addButton (self.mc)
        self.methSelectionRadioGroup.addButton (self.cp)
        self.methSelectionRadioGroup.addButton (self.mcst)
        self.methSelectionRadioGroup.addButton (self.mcvscp)
        self.methodSelection.addWidget (self.mc)
        self.methodSelection.addWidget (self.cp)
        self.methodSelection.addWidget (self.mcst)
        self.methodSelection.addWidget (self.mcvscp)

        self.inputLayout = QtGui.QVBoxLayout()  # input function, limits, etc. layout
        self.dashedLabel = QtGui.QLabel ('---------------------------------------------------------------------------------------------------------------------------------------------------------------')
        #--------function input field--------------------
        self.funcLabel = QtGui.QLabel ('Input the integrand: ')
        self.funcEditWidget = QtGui.QLineEdit ()
        self.inputLayout.addWidget (self.dashedLabel)
        self.inputLayout.addWidget (self.funcLabel)
        self.inputLayout.addWidget (self.funcEditWidget)
        #--------limits input field----------------------
        self.limitsLabel = QtGui.QLabel ('Input the limits of integration: ')
        self.limitsEditWidget = QtGui.QLineEdit ()
        self.inputLayout.addWidget (self.dashedLabel)
        self.inputLayout.addWidget (self.limitsLabel)
        self.inputLayout.addWidget (self.limitsEditWidget)
        #--------nTests for MCM input field----------------------
        self.nTestsLabel = QtGui.QLabel ('Input amount of tests (list or single value) for Monte-Carlo method: ')
        self.nTestsEditWidget = QtGui.QLineEdit ()
        self.inputLayout.addWidget (self.dashedLabel)
        self.inputLayout.addWidget (self.nTestsLabel)
        self.inputLayout.addWidget (self.nTestsEditWidget)
        #--------precisions for RMCP input field----------------------
        self.precisionLabel = QtGui.QLabel ('Input precision(s) (list or single value) for Rectangles method: ')
        self.precisionEditWidget = QtGui.QLineEdit ()
        self.inputLayout.addWidget (self.dashedLabel)
        self.inputLayout.addWidget (self.precisionLabel)
        self.inputLayout.addWidget (self.precisionEditWidget)
        #--------amount of branches input field----------------------
        self.nBranchesLabel = QtGui.QLabel ('Input amount of branches (leave this field empty so system use number of available processors): ')
        self.nBranchesEditWidget = QtGui.QLineEdit ()
        self.inputLayout.addWidget (self.dashedLabel)
        self.inputLayout.addWidget (self.nBranchesLabel)
        self.inputLayout.addWidget (self.nBranchesEditWidget)

        #---------RUN button----------------------
        self.btnRUN = QtGui.QPushButton ("RUN")

        #---------print_results text area----------------------
        self.logOutput = QtGui.QTextEdit ()
        self.logOutput.setReadOnly (True)
        self.logOutput.setLineWrapMode (QtGui.QTextEdit.NoWrap)
        font = self.logOutput.font ()
        font.setFamily ("Courier")
        font.setPointSize (10)
        self.logOutput.setCurrentFont (font)
        sb = self.logOutput.verticalScrollBar ()
        sb.setValue (sb.maximum())

        #---------buttons and layout--------------
        # self.btnPrevious = QtGui.QPushButton ("<<") #add QIcon as 1st argument
        # self.btnPlay = QtGui.QPushButton (">")
        # self.btnPause = QtGui.QPushButton ("||")
        # self.btnStop = QtGui.QPushButton ("[stop]")
        # self.btnNext = QtGui.QPushButton (">>")
        # self.hbox_player_buttons = QtGui.QHBoxLayout ()
        # self.hbox_player_buttons.addWidget (self.btnPrevious)        
        # self.hbox_player_buttons.addWidget (self.btnPlay)        
        # self.hbox_player_buttons.addWidget (self.btnPause)        
        # self.hbox_player_buttons.addWidget (self.btnStop)        
        # self.hbox_player_buttons.addWidget (self.btnNext)

        #-------plot buttons------------
        self.plotLabel = QtGui.QLabel ('Plot graphs for last tests: ')
        self.btnPlotMCS = QtGui.QPushButton ("MC Speed Up")
        self.btnPlotMCRM = QtGui.QPushButton ("MC vs RM")
        self.hboxPlotButtons = QtGui.QHBoxLayout ()
        # self.hbox_adddel_buttons.addStretch (1)
        self.hboxPlotButtons.addWidget (self.btnPlotMCS)
        self.hboxPlotButtons.addWidget (self.btnPlotMCRM)
        # self.hbox_adddel_buttons.addWidget (self.btnDel)
        # #--------playlist and general layout-------------
        # self.playList = QtGui.QListWidget ()
        
        # self.playList.setSelectionMode (QtGui.QAbstractItemView.MultiSelection)
        # self.playList.scrollBarWidgets
        # #self.playList.setVerticalScrollBar()   #argument QScrollBar
        # self.playList.setStyleSheet ("background-color: #408080;")
        # #self.playList.setStyleSheet("background-image:url(:bg15.png);")
        self.vboxGeneral = QtGui.QVBoxLayout ()
        # self.vbox_general.addLayout (self.hbox_player_buttons)
        # self.vbox_general.addWidget (self.playList)
        # self.vbox_general.addLayout (self.hbox_adddel_buttons)
        self.vboxGeneral.addLayout (self.methodSelection)
        self.vboxGeneral.addLayout (self.inputLayout)
        self.vboxGeneral.addWidget (self.btnRUN)
        self.vboxGeneral.addWidget (self.logOutput)
        self.vboxGeneral.addWidget (self.plotLabel)
        self.vboxGeneral.addLayout (self.hboxPlotButtons)
        self.setLayout (self.vboxGeneral)
       
        
        # #-------playlist manipulations---------------
        self.connect (self.btnRUN, QtCore.SIGNAL("clicked()"), QtCore.SLOT("clickedRun()"))
        self.connect (self.btnPlotMCS, QtCore.SIGNAL("clicked()"), QtCore.SLOT("clickedPlotMCS()"))
        # #-------audio buttons manipulations----------
        # self.connect (self.btnPlay, QtCore.SIGNAL("clicked()"), QtCore.SLOT("clicked_play()"))
        # self.connect (self.btnStop, QtCore.SIGNAL("clicked()"), self.on_stop)

    def closeEvent(self, event):    #overloaded
        pass

    @QtCore.pyqtSlot()
    def clickedRun (self):

        self.arguments['MCSpeedup'], self.arguments['MCvsCP'] = False, False
        self.arguments['MonteCarlo'][0], self.arguments['CenterPoint'][0] = False, False
        meth = self.methSelectionRadioGroup.checkedId ()
        if meth == -1:
            QtGui.QMessageBox.question (self, 'InputError: ', 'Select a method, please', QtGui.QMessageBox.Ok)
            return
        self.arguments['function'] = """def f(x): return """ + self.funcEditWidget.text()
        limitsLine = self.limitsEditWidget.text()
        ntestsLine = self.nTestsEditWidget.text()
        precisionLine = self.precisionEditWidget.text()
        nbranchesLine = self.nBranchesEditWidget.text()
        
        self.parseMeth (meth)
        self.arguments['limits'] = self.parseLimits (limitsLine)
        if isinstance(self.arguments['limits'], int): 
            QtGui.QMessageBox.question (self, 'InputError: ', 'Input the limits right way, please', QtGui.QMessageBox.Ok)
            return
        self.arguments['nBranches'] = self.parseNBranches (nbranchesLine)
        if self.arguments['nBranches'] == -1:
            QtGui.QMessageBox.question (self, 'InputError: ', 'Input number of branches correctly, please', QtGui.QMessageBox.Ok)
            return       
        if meth in [-2, -4, -5]:
            self.arguments['MonteCarlo'][1] = self.parseNTests (ntestsLine)
            if isinstance(self.arguments['MonteCarlo'][1], int): 
                QtGui.QMessageBox.question (self, 'InputError: ', 'Uncorrect number of tests for MC', QtGui.QMessageBox.Ok)
                return
        if meth in [-3, -5]:
            self.arguments['CenterPoint'][1] = self.parsePrecisons (precisionLine)
            if isinstance(self.arguments['CenterPoint'][1], int): 
                QtGui.QMessageBox.question (self, 'InputError: ', 'Uncorrect list of precisions for RM', QtGui.QMessageBox.Ok)
                return
        
        try:
            results = []
            results = self.calculatorObject.run (self.arguments)
            self.printResults (results, meth)
            print (results)
        except ValueError as e:
            QtGui.QMessageBox.question (self, 'Calculation Error: ', e, QtGui.QMessageBox.Ok)
        except:
            print ('Unknown error from ResultsHandler object: ', sys.exc_info()[0])
            print('-'*60)
            traceback.print_exc (file = sys.stdout)
            print('-'*60)   #        for key, value in self.arguments.items ():
#            print (key, ':', value)
        return

    @QtCore.pyqtSlot()
    def clickedPlotMCS (self):
        self.plotMCS.plot ('../View/MCSpeedupTest')

    def printResults (self, results, meth):
        try:
            if -2 == meth:  # print MC results [..., [nTests, result, time], ...]
                self.logOutput.append ('### integration results : Monte-Carlo method ###')
                self.logOutput.append ('------------------------------------------------')
                for item in results:
                    self.logOutput.append ('number of tests : ' + str(item[0]))
                    self.logOutput.append ('time            : ' + str(round(item[1], 6)))
                    self.logOutput.append ('result          : ' + str(item[2]))
                    self.logOutput.append ('------------------------------')
            elif -3 == meth: # print CP results [..., [precision, result, time], ...]
                self.logOutput.append ('### integration results : Rectangles CP method ###')
                self.logOutput.append ('--------------------------------------------------')
                for item in results:
                    self.logOutput.append ('precison: ' + str(item[0]))
                    self.logOutput.append ('time   : ' + str(round(item[1], 6)))
                    self.logOutput.append ('result : ' + str(item[2]))
                    self.logOutput.append ('------------------------------')
            elif -4 == meth:    # print MCSpeedup test results  [..., [nTests, Linear[Time, Result], Parallel[Time, Result], Speedup], ...]
                self.logOutput.append ('### speedup test : Monte-Carlo method ###')
                self.logOutput.append ('-----------------------------------------')
                for item in results:
                    self.logOutput.append ('number of tests : ' + str(item[0]))
                    self.logOutput.append ('time Linear     : ' + str(round(item[1][0], 6)))
                    self.logOutput.append ('time Parallel   : ' + str(round(item[2][0], 6)))
                    self.logOutput.append ('result Linear   : ' + str(round(item[1][1], 4)))
                    self.logOutput.append ('result Parallel : ' + str(round(item[2][1], 4)))
                    self.logOutput.append ('Speed Up        : ' + str(round(item[3], 4)))
                    self.logOutput.append ('------------------------------')
            elif -5 == meth:    # print MCvsCP test results  [..., [ [nTests, v, t], [presicion, v, t] ], ...]
                self.logOutput.append ('### comparison test : Monte-Carlo vs Rectangles ###')
                self.logOutput.append ('-----------------------------------------')
                for item in results:
                    self.logOutput.append ('number of tests for MC : ' + str(item[0][0]))
                    self.logOutput.append ('precison value for RM  : ' + str(item[1][0]))
                    self.logOutput.append ('time MC   : ' + str(round(item[0][2], 6)))
                    self.logOutput.append ('time RM   : ' + str(round(item[1][2], 6)))
                    self.logOutput.append ('result MC : ' + str(round(item[0][1], 4)))
                    self.logOutput.append ('result RM : ' + str(round(item[1][1], 4)))
                    self.logOutput.append ('------------------------------')
            else: raise
        except:
            QtGui.QMessageBox.question (self, 'Results output Error: ', 'AppError: Unknown results format', QtGui.QMessageBox.Ok)

    def parseMeth (self, meth):
        if -2 == meth:
            self.arguments['MonteCarlo'][0] = True
        elif -3 == meth:
            self.arguments['CenterPoint'][0] = True
        elif -4 == meth:
            self.arguments['MCSpeedup'] = True
        else:
            self.arguments['MCvsCP'] = True

    def parseLimits (self, lims):
        lims = lims.split(';')
        limits = []
        for lim in lims:
            lim = lim.split(',')
            try:
                if lim:
                    lim[0] = float(lim[0])
                    lim[1] = float(lim[1])
                else: continue
            except:
                return -1
            limits.append (lim)
        if not limits: return -1
        return limits

    def parseNBranches (self, nBranches):
        try:
            if not nBranches:
                nBranches = 0
            else:
                nBranches = int (nBranches)
                if nBranches < 0:
                    raise
        except:
            return -1
        return nBranches

    def parseNTests (self, nTests):
        nTests = nTests.split(',')
        try:
            if nTests:
                nTests = [int(p) for p in nTests if p]
        except:
            return -1
        if not nTests: return -1
        return nTests

    def parsePrecisons (self, precisions):
        precisions = precisions.split(',')
        try:
            if precisions:
                precisions = [float(p) for p in precisions if p]
        except:
            return -1
        if not precisions: return -1
        return precisions



if __name__ == '__main__':

        import signal 
        signal.signal (signal.SIGINT, signal.SIG_DFL)

        app = QtGui.QApplication(sys.argv)
        
        gui = GUI ()
        gui.show()

        app.exec_()
